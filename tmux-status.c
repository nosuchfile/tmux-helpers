#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

static int debug;
static int ncores;

static char bar_levels[][4] = {
    "_",
    "\342\226\201",
    "\342\226\202",
    "\342\226\203",
    "\342\226\204",
    "\342\226\205",
    "\342\226\206",
    "\342\226\207",
    "\342\226\210",
};

static int bar_level_max = ARRAY_SIZE(bar_levels);

struct tmux_status_ctx {
    int  level_fg_color[3];
    int  level_bg_color[3];
    int  default_fg_color;
    int  default_bg_color;
    int  delim_color;

    char buffer[512];
    int  buffer_len;
};

typedef int (*tmux_status_element_t)(struct tmux_status_ctx *ctx);

static int tmux_delim(struct tmux_status_ctx *ctx);

static int tmux_printf(struct tmux_status_ctx *ctx,
                       char const             *fmt,
                       ...)
{
    int     ret;
    va_list ap;

    va_start(ap, fmt);
    ret = vsnprintf(ctx->buffer + ctx->buffer_len,
                    sizeof(ctx->buffer) - ctx->buffer_len,
                    fmt, ap);
    va_end(ap);

    if(ret > 0) {
        ctx->buffer_len += (size_t)ret < sizeof(ctx->buffer) - ctx->buffer_len ?
            (size_t)ret :
            sizeof(ctx->buffer) - ctx->buffer_len;;
    }

    return ret;
}

static int tmux_pcap(struct tmux_status_ctx *ctx)
{
    FILE *fp;
    int  count = 0;

    fp = popen("fasttop -t -n 1", "r");
    if(fp) {
        char line[BUFSIZ];

        if(fgets(line, sizeof(line), fp) != NULL) {
            while(fgets(line, sizeof(line), fp) != NULL) {
                char *s = strrchr(line, ' ');
                if(s) {
                    char *e;

                    while(*s == ' ') {
                        s++;
                    }

                    e = s + strlen(s) - 1;
                    while(*e == '\n') {
                        *(e--) = '\0';
                    }

                    tmux_printf(ctx, "%s%s", count > 0 ? " " : "", s);
                    count++;
                }
            }
        }
        pclose(fp);
    }

    return count == 0 ? 1 : 0;
}

static int tmux_set_bg(struct tmux_status_ctx *ctx,
                       int bg_color)
{
    if (debug) {
        return tmux_printf(ctx, "\e[38;5;%dm", bg_color);
    }
    return tmux_printf(ctx, "#[bg=colour%d]", bg_color);
}

static int tmux_set_fg(struct tmux_status_ctx *ctx,
                       int fg_color)
{
    if (debug) {
        return tmux_printf(ctx, "\e[38;5;%dm", fg_color);
    }
    return tmux_printf(ctx, "#[fg=colour%d]", fg_color);
}

static int tmux_set_fg_and_bg(struct tmux_status_ctx *ctx,
                              int fg_color, int bg_color)
{
    if (debug) {
        return tmux_printf(ctx, "\e[48;5;%dm\e[38;5;%dm",
                           bg_color, fg_color);
    }
    return tmux_printf(ctx, "#[fg=colour%d,bg=colour%d]",
                       fg_color, bg_color);
}

static int tmux_loadavg(struct tmux_status_ctx *ctx)
{
    FILE *fp;

    fp = fopen("/proc/loadavg", "r");
    if(fp) {
        char line[BUFSIZ];

        if(fgets(line, sizeof(line), fp) != NULL) {
            char *p;
            int   int_load[3];
            int   dec_load[3];
            int   running;
            int   processes;

            if(sscanf(line, "%d.%d %d.%d %d.%d %d/%d %*d",
                      &int_load[0], &dec_load[0],
                      &int_load[1], &dec_load[1],
                      &int_load[2], &dec_load[2],
                      &running, &processes) == 8) {

                int i;

                for(i = 0; i < 3; i++) {
                    int     level;

                    if(int_load[i] >= ncores * 2) {
                        level = 2;
                    } else if(int_load[i] >= ncores) {
                        level = 1;
                    } else {
                        level = 0;
                    }

                    int l = int_load[i] * 2 / ncores;
                    if (l > ARRAY_SIZE(bar_levels) - 1) {
                        l = ARRAY_SIZE(bar_levels) - 1;
                    }

                    tmux_set_fg_and_bg(ctx,
                                       ctx->level_fg_color[level],
                                       ctx->level_bg_color[level]);
                    tmux_printf(ctx, "%s", bar_levels[l]);
                    tmux_set_bg(ctx, ctx->default_bg_color);
                }
            }

        }

        fclose(fp);
    }

    return 0;
}

static int tmux_memfree(struct tmux_status_ctx *ctx)
{
    FILE *fp;

    fp = fopen("/proc/meminfo", "r");
    if(fp) {
        char line[BUFSIZ];
        int tag = 0;
        uint32_t total_mem;
        uint32_t free_mem;
        uint32_t buffered_mem;
        uint32_t cached_mem;

        while(fgets(line, sizeof(line), fp) != NULL) {
            if(sscanf(line, "MemTotal: %d kB", &total_mem) == 1) {
                tag++;
            } else if(sscanf(line, "MemFree: %d kB", &free_mem) == 1) {
                tag++;
            } else if(sscanf(line, "Buffers: %d kB", &buffered_mem) == 1) {
                tag++;
            } else if(sscanf(line, "Cached: %d kB", &cached_mem) == 1) {
                tag++;
            }

            if(tag == 4) {
                int used_mem = total_mem - (free_mem + buffered_mem + cached_mem);

                tmux_printf(ctx, "\357\213\233 %d%%", 100 * used_mem / total_mem);
                break;
            }
        }

        fclose(fp);
    }

    return 0;
}

static int tmux_battery(struct tmux_status_ctx *ctx)
{
    FILE *p;
    char buffer[1024];
    char *line;

    p = popen("acpi -b", "r");
    if (p == NULL) {
        return 0;
    }

    line = fgets(buffer, sizeof(buffer), p);
    pclose(p);

    if (line) {
        char *p;
        char *q;
        int percent;
        int idx;

        p = strchr(line, '%');
        if (p == NULL) {
            return 0;
        }
        q = p;
        while(q > line && *q != ' ') {
            q--;
        }
        q++;
        percent = strtoll(q, NULL, 10);

        char icon[10];
        if (percent < 12) {
            strcpy(icon, "\357\211\204 ");
            idx = 2;
        } else if (percent < 25) {
            strcpy(icon, "\357\211\203 ");
            idx = 2;
        } else if (percent < 50) {
            strcpy(icon, "\357\211\202 ");
            idx = 1;
        } else if (percent < 75) {
            strcpy(icon, "\357\211\201 ");
            idx = 0;
        } else {
            strcpy(icon, "\357\211\200 ");
            idx = 0;
        }
        tmux_set_fg(ctx, ctx->level_fg_color[idx]);
        tmux_printf(ctx, "%s", icon);
        tmux_set_fg(ctx, ctx->default_fg_color);
    }

    return 0;
}

static int tmux_delim(struct tmux_status_ctx *ctx)
{
    tmux_set_fg_and_bg(ctx,
                       ctx->delim_color, ctx->default_bg_color);
    tmux_printf(ctx, " ⎜");
    tmux_set_fg(ctx, ctx->default_fg_color);

    return 0;
}

static tmux_status_element_t tmux_status[] = {
    tmux_loadavg,
    tmux_delim,
    tmux_memfree,
    tmux_delim,
    tmux_battery,
    tmux_delim,
    NULL,
};

static void tmux_status_from_env(struct tmux_status_ctx *ctx)
{
    char *env;

    env = getenv("TMUX_STATUS_BG");
    if (env) {
        int color = atoi(env);
        ctx->default_bg_color = color;
        ctx->level_bg_color[0] = color;
        ctx->level_bg_color[1] = color;
        ctx->level_bg_color[2] = color;
    }

    env = getenv("TMUX_STATUS_FG");
    if (env) {
        int color = atoi(env);
        ctx->default_fg_color = color;
        ctx->level_fg_color[0] = color;
    }

    env = getenv("TMUX_STATUS_DELIM");
    if (env) {
        int color = atoi(env);
        ctx->delim_color = color;
    }
}

int main(int argc, const char *argv[])
{
    struct tmux_status_ctx ctx = {
        .level_fg_color   = { 242, 136, 124 },
        .level_bg_color   = { 235, 235, 235 },
        .default_fg_color = 242,
        .default_bg_color = 235,
        .delim_color      = 237,

        .buffer_len       = 0,
    };
    int i;
    int skip_next = 0;

    if (argc > 1 && strcmp(argv[1], "-d") == 0) {
        debug = 1;
    }

    ncores = sysconf(_SC_NPROCESSORS_ONLN);

    tmux_status_from_env(&ctx);

    for(i = 0; tmux_status[i]; i++) {
        if(skip_next == 0) {
            skip_next = tmux_status[i](&ctx);
        } else {
            skip_next = 0;
        }
    }

    if(ctx.buffer_len > 0) {
        write(1, ctx.buffer, ctx.buffer_len);

        fflush(stdout);
    }

    return 0;
}
